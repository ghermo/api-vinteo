<?php

class DevicesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function userCanPostDevice()
    {
        $this->visit('api/v1/devices')
            ->see('Hello World!');
    }
}
