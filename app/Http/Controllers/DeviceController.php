<?php
/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 6/28/15
 * Time: 5:42 PM
 */

namespace App\Http\Controllers;

use App\Device;
use App\Pcounter;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DeviceController extends Controller
{

    public function index()
    {

        return Device::all();

    }
/**
 * Check Status of all devices
 * @return [type] [description]
 */
    public function status()
    {
        $result = Pcounter::groupBy('device_id')
            ->select(DB::raw('MAX(created_at) as latestdate'), 'device_id')
            ->orderBy('created_at', 'DESC')
            ->get();

        $data = [];

        foreach ($result as $key => $value) {
            if ($value->device_id != '') {
                $d = [];
                $d['id'] = $value->device_id;
                $d['lastactivity'] = Carbon::parse($value->latestdate)->diffForHumans();
                $d['lastactivitydate'] = $value->latestdate;
                $data[] = $d;
            }
        }

        return response()->json($data);
    }

    /**
     *  Compute time the elapsed
     */

    public function timeElapsedString($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
