<?php
/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 6/28/15
 * Time: 5:42 PM
 */

namespace App\Http\Controllers;

use App\Transaction;
use App\Pcounter;
use Illuminate\Http\Request;
use Validator;

class TransactionController extends Controller
{

    public function index()
    {

        return Transaction::all();

    }

    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'data'       => ['required']
        ]);

        if ($v->fails()) {
            return $v->errors()->all();
        }

        $data = $request->input('data');

        $this->countPerson($data);

    }

    protected function countPerson($data)
    {


        $jsondata = json_decode($data);

        if (is_null($jsondata)) {
            dd('not valid json data');
        }

        $now = date('Y-m-d');

        // dd(json_decode($data));

            $d = json_decode($data);

                $pcounter = new Pcounter();
                $pcounter->countab = $d->countAB;
                $pcounter->countba = $d->countBA;
                $pcounter->starttime = $d->date . ' ' . $d->startTime;
                $pcounter->endtime = $d->date . ' ' . $d->endTime;
                $pcounter->save();

        

        dd(json_decode($data));

    }

    public function checkTimeString($time)
    {

        $regex = '/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/';
        if (preg_match($regex, $time)) {
            return true;
        } else {
            return false;
        }

    }
}
