<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->welcome();
});

$app->get('devices/status', ['uses' => 'DeviceController@status']);

/*
 * API
 */

$app->group([
        'prefix' => 'api/v1',
        'namespace' => 'App\Http\Controllers',
        'middleware' => 'oauth',
        ], function () use ($app) {

            $app->get('devices', ['uses' => 'DeviceController@index']);
            $app->get('transactions', ['uses' => 'TransactionController@index']);
            $app->get('peoplecount', ['uses' => 'PeopleCountController@index']);
            $app->post('transactions', ['uses' => 'TransactionController@store']);

        });

/*
 * oauth2
 */

$app->post('login', function () use ($app) {
    $credentials = app()->make('request')->input('credentials');

    return $app->make('App\Auth\Proxy')->attemptLogin($credentials);
});

$app->post('refresh-token', function () use ($app) {
    return $app->make('App\Auth\Proxy')->attemptRefresh();
});

$app->post('oauth/access-token', function () use ($app) {
    return response()->json($app->make('oauth2-server.authorizer')->issueAccessToken());
});
