<?php
/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 6/28/15
 * Time: 10:22 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionTableSeeder extends Seeder {
  /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('transactions')->truncate();

        $date = new DateTime();
        $date->sub(new DateInterval('P1D'));

        $cdate = $date->format('m-d-Y');


        foreach(range(1,100) as $index) {

        $date->add(new DateInterval('PT1S'));
        $timestart = $date->format('H:i:s');
        $date->add(new DateInterval('PT30S'));
        $timeend = $date->format('H:i:s');

            DB::table('transactions')->insert([
                'device_id' => $faker->macAddress(),
                'msg' => "{'startTime':$timestart,'endTime':$timeend,'date':$cdate,'countBA':$faker->randomDigit,'countAB':$faker->randomDigit}",
            ]);
        }
    }
}