<?php
/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 6/28/15
 * Time: 10:22 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeviceTableSeeder extends Seeder {
  /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('devices')->truncate();

        foreach(range(1,50) as $index) {

            DB::table('devices')->insert([
                'device_id' => $faker->macAddress(),
                'inside_state' => $faker->randomElement(['A','B']),
                'outside_deploy' => $faker->randomElement(['Y','N']),
            ]);
        }
    }
}